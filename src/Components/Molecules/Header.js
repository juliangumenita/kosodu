import React from "react";

import Text from "../Atoms/Text";

const Component = ({ children, bottom }) => (
  <Text size={48} line={60} weight="600" block bottom={bottom}>
    {children}
  </Text>
);

export default Component;
