import React from "react";

import Text from "../Atoms/Text";
import Spacer from "../Atoms/Spacer";

const Component = ({ children }) => (
  <Spacer right={24} left={24}>
    <Text size={16} line={19} weight="600">
      {children}
    </Text>
  </Spacer>
);

export default Component;
