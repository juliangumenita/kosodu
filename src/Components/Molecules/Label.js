import React from "react";

import Text from "../Atoms/Text";

const Component = ({ children, bottom }) => (
  <Text size={14} line={20} weight="500" opacity={0.3} block bottom={bottom}>
    {children}
  </Text>
);

export default Component;
