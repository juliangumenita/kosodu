import React from "react";

import {
  StaticContainer,
  NavbarContainer,
  FooterContainer,
} from "../Containers";
import { API, Resources } from "../Modules";

import { Consent } from "../Components";

class Component extends React.PureComponent {
  componentDidMount() {
    var key;

    switch (this.props.match.params.static) {
      case "privacy-policy":
        key = Resources.PRIVACY_POLICY;
        break;
      case "terms-of-use":
        key = Resources.TERMS_OF_USE;
        break;
      default:
        key = Resources.PRIVACY_POLICY;
    }

    API.static(key);
  }

  render() {
    return (
      <div>
        <header>
          <div
            className="container"
            margin="large"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <a href="/">
              <img src="/assets/i/logo.svg" className="logo" />
            </a>
            <a href="/blog">
              <div className="switch">
                <div className="switch__item">Blog</div>
              </div>
            </a>
          </div>
        </header>
        <StaticContainer />
        <div className="footer">
          <div className="container">
            <footer>
              <div>
                <p>© Kosodu 2020</p>
                <p>info@kosodu.com</p>
              </div>
              <div flex="flex" align="center" spacing="spacing">
                <a href="/static/privacy-policy" className="menu__item">
                  Gizlilik Sözleşmesi
                </a>
                <a href="/static/terms-of-use" className="menu__item">
                  Kullanım Koşulları
                </a>
              </div>
            </footer>
          </div>
        </div>
        <Consent />
      </div>
    );
  }
}

export default Component;
