export { default as HomeScreen } from "./HomeScreen";
export { default as BlogScreen } from "./BlogScreen";
export { default as PostScreen } from "./PostScreen";
export { default as StaticScreen } from "./StaticScreen";
