import React from "react";
import { ScreenClassRender } from "react-grid-system";

import { Padding, Button, Align, Logo, Text } from "../Components";
import { Colors } from "../Helpers";
import { Container } from "react-grid-system";
import { Link } from "react-router-dom";

class Component extends React.PureComponent {
  render() {
    return (
      <ScreenClassRender
        render={(screen) =>
          ["xs", "sm"].includes(screen) ? (
            <Container>
              <Padding top={16} bottom={16 + 24} display="block">
                <Align align="center" justify="space-between">
                  <Logo />
                  {this.props["no-blog"] ? null : (
                    <Link to="/blog">
                      <Text weight="600" size={20} color={Colors.russian}>
                        Blog
                      </Text>
                    </Link>
                  )}
                </Align>
              </Padding>
            </Container>
          ) : (
            <Container>
              <Padding top={48} bottom={48 + 60} display="block">
                <Align align="center" justify="space-between">
                  <Logo />
                  {this.props["no-blog"] ? null : (
                    <Link to="/blog">
                      <Button>Blog</Button>
                    </Link>
                  )}
                </Align>
              </Padding>
            </Container>
          )
        }
      ></ScreenClassRender>
    );
  }
}

export default Component;
