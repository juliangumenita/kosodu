const Partners = [
  {
    url: "https://internethizi.com/",
    name: "internethızı.com   ",
  },
  {
    url: "https://speedot.net/",
    name: "speedot.net   ",
  },
  {
    url: "https://zekathesapla.net/",
    name: "zekathesapla.net   ",
  },
  {
    url: "https://zekathesaplama.com/",
    name: "zekathesaplama.com   ",
  },
  {
    url: "https://kronometre.co/",
    name: "kronometre.co   ",
  },
  {
    url: "https://randomsites.com/",
    name: "randomsites.com   ",
  },
  {
    url: "https://rastgelesiteler.com/",
    name: "rastgelesiteler.com   ",
  },
  {
    url: "https://motivationim.com/",
    name: "motivationim.com",
  },
];

export default Partners;
