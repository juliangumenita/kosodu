const Types = {
  helpful: "Yararlı / Eğitici",
  fun: "Eğlenceli / Komik",
  interesting: "Şaşırtıcı / Amaçız",
  consuming: "Vakit Öldürücü / Zaman Geçirici"
};

export default Types;
