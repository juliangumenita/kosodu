const Resources = {
  PRIVACY_POLICY: "Kosodu Privacy Policy",
  TERMS_OF_USE: "Kosodu Terms of Use"
};

export default Resources;
