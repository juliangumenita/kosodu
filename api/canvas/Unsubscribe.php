<?php
  use Bitter\Form;

  $emails = json_decode(
    file_get_contents("public/emails.json"),
    true
  );
  $founds = json_decode(
    file_get_contents("public/founds.json"),
    true
  );
  $found = $founds[Form::get("email")];

  foreach($emails as $key => $email){
    if($email == $found){
      unset($emails[$key]);

      file_put_contents(
        "public/emails.json",
        json_encode($emails, JSON_UNESCAPED_UNICODE)
      );
    }
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8"/>
    <title>Koronavirüs Son Durumu - Kosodu</title>
    <link rel="stylesheet" href="/assets/css/style.css"/>
    <link rel="shortcut icon" href="/assets/i/favicon.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Korona (Corona) (Covid19) Son Durumunu Veren Türkiye'nin Platformu."/>
    <script src="https://unpkg.com/feather-icons" charset="utf-8"></script>
    <script src="https://unpkg.com/jquery" charset="utf-8"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://unpkg.com/chart.js" charset="utf-8"></script>
    <style media="screen">
      html{
        widows: 100%;
        height: 100%;
      }
      body, html{
        widows: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }
    </style>
  </head>
  <body>
    <h1 class="display" id="display" margin="large">Görüşürüz</h1>
    <a href="https://kosodu.com" class="button">Siteye Geri Dön</a>
  </body>
</html>
