<?php
  use Bitter\API;
  use Bitter\Route;

  Route::canvas("/subscribe", "Subscribe");
  Route::canvas("/unsubscribe", "Unsubscribe");
  API::post("/daily", "Email@daily");
  API::post("/count", "Email@count");
?>
