<?php
  define("MAILGUN_KEY", "9309991f5ff4fcbfe43ef6534c5b87b5-2b0eef4c-7f44fd5c");
  define("MAILGUN_URL", "https://api.eu.mailgun.net/v3/mg.kosodu.com");
  class Mailer{
    public static function send($to, $subject, $message){
      self::mailgun($to, $subject, $message);
    }

    public static function mailgun($to, $subject, $message){
      $data = array(
        "from"=> "Kosodu <support@kosodu.com>",
        "to"=> $to,
        "subject" => $subject,
        "html" => $message
      );

      $session = curl_init(MAILGUN_URL . "/messages");
      curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($session, CURLOPT_USERPWD, "api:" . MAILGUN_KEY);
      curl_setopt($session, CURLOPT_POST, true);
      curl_setopt($session, CURLOPT_POSTFIELDS, $data);
      curl_setopt($session, CURLOPT_HEADER, false);
      curl_setopt($session, CURLOPT_ENCODING, "UTF-8");
      curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);

      $result = curl_exec($session);
      $result = json_decode($result, 1);
      curl_close($session);
      return $result;
    }
  }
?>
