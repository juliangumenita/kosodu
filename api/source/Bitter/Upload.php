<?php
  namespace Bitter;

  class Upload{
    public static function image($file, $path, $width = 500, $height = 500, $bypass = false){
      if(isset($_FILES[$file])){
        if($_FILES[$file]["size"] < 10485760){
          move_uploaded_file($_FILES["image"]["tmp_name"], $path);

          if($bypass){
            self::resize($width, $path, $path);
          }

          if(!$bypass){
            self::crop($width, $height, $path, $path);
          }
        } return false;
      } return false;
    }

    public static function base64($file, $path, $width = 500, $height = 500, $bypass = false){
      if(substr($file, 0, strlen("data:image/jpeg;base64,")) !== "data:image/jpeg;base64,"){
        $file = "data:image/jpeg;base64," . $file;
      }

      $ifp = fopen($path, "wb");
      $data = explode(",", $file);

      fwrite(
        $ifp,
        base64_decode(
          $data[1]
        )
      );

      fclose($ifp);

      if($bypass){
        self::resize($width, $path, $path);
      }

      if(!$bypass){
        self::crop($width, $height, $path, $path);
      }

      return true;
    }

    private static function resize($width, $target, $file){
      $info = getimagesize($file);
      $mime = $info["mime"];

      switch($mime){
        case "image/jpeg":
          $create = "imagecreatefromjpeg";
          $save = "imagejpeg";
          $extension = "jpg";
          break;

        case "image/png":
          $create = "imagecreatefrompng";
          $save = "imagepng";
          $extension = "png";
          break;

        case "image/gif":
          $create = "imagecreatefromgif";
          $save = "imagegif";
          $extension = "gif";
          break;

        default:
          throw new Exception('Unknown image type.');
      }

      $image = $create($file);
      list($width, $height) = getimagesize($file);

      $height__ = ($height / $width) * $width;
      $tmp = imagecreatetruecolor($width, $height__);
      imagecopyresampled($tmp, $image, 0, 0, 0, 0, $width, $height__, $width, $height);

      if (file_exists($target)) {
              unlink($target);
      }
      $save($tmp, $target);
    }

    private static function crop($max_width, $max_height, $file, $dst_dir, $quality = 80){
      $imgsize = getimagesize($file);
      $width = $imgsize[0];
      $height = $imgsize[1];
      $mime = $imgsize['mime'];

      switch($mime){
        case "image/gif":
          $create = "imagecreatefromgif";
          $image = "imagegif";
          break;

        case "image/png":
          $create = "imagecreatefrompng";
          $image = "imagepng";
          $quality = 7;
          break;

        case "image/jpeg":
          $create = "imagecreatefromjpeg";
          $image = "imagejpeg";
          $quality = 80;
          break;

        default:
          return false;
          break;
      }

      $destination = imagecreatetruecolor($max_width, $max_height);
      $source = $create($file);

      $width_new = $height * $max_width / $max_height;
      $height_new = $width * $max_height / $max_width;

      if($width_new > $width){
        $h_point = (($height - $height_new) / 2);

        imagecopyresampled($destination, $source, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
      } else {
        $w_point = (($width - $width_new) / 2);

        imagecopyresampled($destination, $source, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
      }

      $image($destination, $dst_dir, $quality);

      if($destination){
        imagedestroy($destination);
      }

      if($source){
        imagedestroy($source);
      }

      return true;
    }
  }
?>
