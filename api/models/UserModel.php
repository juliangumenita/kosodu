<?php
  use Bitter\Model;

  class UserModel extends Model{
    public function __construct(){
      $this->table = "users";

      $this->add("name", self::STRING);
      $this->add("surname", self::STRING);
      $this->add("phone", self::STRING);
    }

    public function build($key, $value){
      $object = $this->entity($key, $value);
      
      $this->set("name", "Julian");
      $this->set("surname", "Gumenita");
      $this->set("phone", "+905394287838");
    }
  }
?>
