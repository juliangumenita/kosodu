<?php
  use Bitter\Controller;
  use Bitter\Template;

  class EmailController extends Controller{
    public function count(){
      $emails = json_decode(
        file_get_contents("public/emails.json"),
        true
      );

      $this->data(
        217 + count($emails)
      );

      return $this->success();
    }

    public function register(){
      $email = $this->request["email"];

      if($email){
        $emails = json_decode(
          file_get_contents("public/emails.json"),
          true
        );
        $founds = json_decode(
          file_get_contents("public/founds.json"),
          true
        );


        if(!in_array($email, $emails)){
          array_push($emails, $email);
          $founds[md5($email)] = $email;
        }

        file_put_contents(
          "public/emails.json",
          json_encode($emails, JSON_UNESCAPED_UNICODE)
        );

        file_put_contents(
          "public/founds.json",
          json_encode($founds, JSON_UNESCAPED_UNICODE)
        );
      }

      return $this->success();
    }

    public function send(){
      $emails = json_decode(
        file_get_contents("public/emails.json"),
        true
      );

      $data = json_decode(
        file_get_contents("https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json"),
        true
      );

      $stats = end($data);

      $html = Template::get("mail", [
        "total_cases" => $stats["totalCases"],
        "total_recovered" => $stats["totalRecovered"],
        "total_deaths" => $stats["totalDeaths"],
        "total_tests" => $stats["totalTests"],
        "new_cases" => $stats["cases"],
        "new_recovered" => $stats["recovered"],
        "new_deaths" => $stats["deaths"],
        "new_tests" => $stats["tests"],
        "date" => date("Y-m-d"),
        "paragraph" => $paragraph,
        "email" => md5($email)
      ]);

      $paragraph = "%new_cases% Yeni Vaka ile toplam vaka sayısı %total_cases% olmuş durumda.";
      $paragraph = str_replace("%new_cases%", $stats["cases"], $paragraph);
      $paragraph = str_replace("%total_cases%", $stats["totalCases"], $paragraph);

      foreach($emails as $key => $email){
        Mailer::send($email, "Türkiye Korona Son Durumu - " . date("d m Y"), $html);
      }

      return $this->success();
    }

    public function daily(){
      $date = file_get_contents("public/date");

      $data = json_decode(
        file_get_contents("https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json"),
        true
      );
      $stats = end($data);
      $key = $stats["date"];

      if($key !== $date){
        $date = file_put_contents("public/date", $key);
        $this->send();
      }

      return $this->success();
    }
  }
?>
