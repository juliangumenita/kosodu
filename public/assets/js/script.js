feather.replace();

function GraphData(labels, datasets) {
  return {
    type: "line",
    data: {
      labels: labels,
      datasets: datasets,
    },
    options: {
      gridLines: {
        display: false,
      },
      scales: {
        yAxes: [
          {
            gridLines: {
              color: "rgba(0, 0, 0, 0.05)",
            },
          },
        ],
        xAxes: [
          {
            gridLines: {
              display: false,
              color: "rgba(0, 0, 0, 0.05)",
            },
          },
        ],
      },
    },
  };
}

function table() {
  $("#logaritmic_view").hide();
  $("#logaritmic").hide();
  $("#generic").show();
}

function graph() {
  $("#logaritmic_view").show();
  $("#logaritmic").show();
  $("#generic").hide();
}

$(".switch__item").click(function (e) {
  if ($(this).attr("role") === "disabled") {
    e.preventDefault();
    return;
  }

  $(this).parent().children(".switch__item").removeClass("active");
  $(this).addClass("active");
});

async function countries() {
  var json = await fetch("https://api.covid19api.com/summary");
  var data = await json.json();

  data.Countries.sort(function (a, b) {
    return b.TotalConfirmed - a.TotalConfirmed;
  });
  return data;
}

function virtual(text, dom = "div", style = null) {
  var virtual_ = document.createElement(dom);
  virtual_.innerHTML = text;

  if (style !== null) {
    virtual_.classList.add(style);
  }

  return virtual_;
}

function commas(nStr) {
  nStr += "";
  var x = nStr.split(".");
  var x1 = x[0];
  var x2 = x.length > 1 ? "." + x[1] : "";
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, "$1" + "." + "$2");
  }
  return x1 + x2;
}

function name(country) {
  if (country.Country === undefined) {
    return "🌍 Dünya";
  } else {
    return country.CountryCode.toUpperCase().replace(/./g, (char) =>
      String.fromCodePoint(char.charCodeAt(0) + 127397)
    ) +
      " " +
      Countries[country.CountryCode.toUpperCase()] ===
      undefined
      ? country.Country
      : Countries[country.CountryCode.toUpperCase()];
  }
}

function row(country) {
  var slug = country.Country === "Turkey" ? "--selected" : "--not--selected";

  if (country.Country === "Turkey") {
    country = window.COUNTRY_FETCHED_LAST;

    var row_ = virtual(null, "tr", "line");

    row_.appendChild(virtual("🇹🇷Türkiye", "td", "column"));
    row_.appendChild(virtual(commas(window.TURKEY_TOTALCASE), "td", "column"));
    row_.appendChild(
      virtual("+" + commas(window.TURKEY_CASE), "td", "column--red")
    );
    row_.appendChild(
      virtual(commas(window.TURKEY_TOTALDEATHS), "td", "column")
    );
    row_.appendChild(
      virtual("+" + commas(window.TURKEY_DEATHS), "td", "column--yellow")
    );

    row_.classList.add(slug);

    document.getElementById("latest").appendChild(row_);
  } else {
    var row_ = this.virtual(null, "tr", "line");
    row_.appendChild(this.virtual(this.name(country), "td", "column"));
    row_.appendChild(
      this.virtual(this.commas(country.TotalConfirmed), "td", "column")
    );
    row_.appendChild(
      this.virtual("+" + this.commas(country.NewConfirmed), "td", "column--red")
    );
    row_.appendChild(
      this.virtual(this.commas(country.TotalDeaths), "td", "column")
    );
    row_.appendChild(
      this.virtual("+" + this.commas(country.NewDeaths), "td", "column--yellow")
    );

    row_.classList.add(slug);

    document.getElementById("latest").appendChild(row_);
  }
}

function historical(day, d, data) {
  var row_ = this.virtual(null, "tr", "line");

  row_.appendChild(
    this.virtual(
      new moment(day.date, "DD/MM/YYYY").format("D MMMM YYYY") === null
        ? day.date
        : new moment(day.date, "DD/MM/YYYY").format("D MMMM YYYY"),
      "td",
      "column"
    )
  );

  row_.appendChild(
    this.virtual(
      "+" + this.commas(data[d + 1] === undefined ? 0 : day.cases),
      "td",
      "column--red"
    )
  );

  row_.appendChild(
    this.virtual(
      "+" + this.commas(data[d + 1] === undefined ? 0 : day.recovered),
      "td",
      "column--green"
    )
  );
  row_.appendChild(
    this.virtual(
      "+" + this.commas(data[d + 1] === undefined ? 0 : day.deaths),
      "td",
      "column--yellow"
    )
  );

  document.getElementById("generic").appendChild(row_);
}

function hydrate() {
  document.getElementById("dinamic--today").innerHTML =
    new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
      .locale("tr")
      .format("D MMMM YYYY") === null
      ? window.COUNTRY[window.COUNTRY_INDEX].date
      : new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
          .locale("tr")
          .format("D MMMM YYYY");
  document.getElementById("today-button").innerHTML =
    new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
      .locale("tr")
      .format("D MMMM YYYY") === null
      ? window.COUNTRY[window.COUNTRY_INDEX].date
      : new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
          .locale("tr")
          .format("D MMMM YYYY");
  document.getElementById("yesterday-button").innerHTML =
    new moment(window.COUNTRY[window.COUNTRY_INDEX - 1].date, "DD/MM/YYYY")
      .locale("tr")
      .format("D MMMM YYYY") === null
      ? window.COUNTRY[window.COUNTRY_INDEX - 1].date
      : new moment(window.COUNTRY[window.COUNTRY_INDEX - 1].date, "DD/MM/YYYY")
          .locale("tr")
          .format("D MMMM YYYY");
  document.getElementById("compare--date").innerHTML =
    new moment(window.COUNTRY[window.COUNTRY_INDEX - 1].date, "DD/MM/YYYY")
      .locale("tr")
      .format("D MMMM YYYY") === null
      ? window.COUNTRY[window.COUNTRY_INDEX].date
      : new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
          .locale("tr")
          .format("D MMMM YYYY");
  document.getElementById("statistic--today").innerHTML =
    new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
      .locale("tr")
      .format("D MMMM YYYY") === null
      ? window.COUNTRY[window.COUNTRY_INDEX].date
      : new moment(window.COUNTRY[window.COUNTRY_INDEX].date, "DD/MM/YYYY")
          .locale("tr")
          .format("D MMMM YYYY");
  document.getElementById("dinamic--date-new").innerHTML = this.commas(
    window.COUNTRY[window.COUNTRY_INDEX].cases
  );
}

function next() {
  if (window.COUNTRY_INDEX < window.COUNTRY.length - 1) {
    window.COUNTRY_INDEX = window.COUNTRY_INDEX + 1;
    this.hydrate();
  }
}

function prev() {
  if (window.COUNTRY_INDEX > 0) {
    window.COUNTRY_INDEX = window.COUNTRY_INDEX - 1;
    this.hydrate();
  }
}

async function current() {
  $.ajax({
    url: "https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json",
    success: function (data) {
      var country = JSON.parse(data);
      var temporary = [];

      for (var k in country) {
        temporary.push(country[k]);
      }

      country = temporary;

      window.COUNTRY = country;
      window.COUNTRY_INDEX = country.length - 1;

      engine();
      hydrate();
    },
  });
}

function dynamic(key, value, numeric = false) {
  if (numeric) {
    try {
      document.getElementById("dinamic--" + key).innerHTML = this.commas(value);
    } catch (e) {
    } finally {
    }
  } else {
    try {
      document.getElementById("dinamic--" + key).innerHTML = value;
    } catch (e) {
    } finally {
    }
  }
}

function total(patients, deaths, recovered) {
  this.dynamic("country-cases", commas(patients));
  this.dynamic("country-deaths", commas(deaths));
  this.dynamic("country-recovered", commas(recovered));

  this.dynamic("country-cases__label", "Toplam Vaka");
  this.dynamic("country-deaths__label", "Toplam Ölüm");
  this.dynamic("country-recovered__label", "Toplam İyileşen");
}

function today(patients, deaths, recovered) {
  this.dynamic("country-cases", commas(patients));
  this.dynamic("country-deaths", commas(deaths));
  this.dynamic("country-recovered", commas(recovered));

  this.dynamic("country-cases__label", "Yeni Vaka");
  this.dynamic("country-deaths__label", "Yeni Ölüm");
  this.dynamic("country-recovered__label", "Yeni İyileşen");
}

function yesterday(patients, deaths, recovered) {
  this.dynamic("country-cases", commas(patients));
  this.dynamic("country-deaths", commas(deaths));
  this.dynamic("country-recovered", commas(recovered));

  this.dynamic("country-cases__label", "Yeni Vaka");
  this.dynamic("country-deaths__label", "Yeni Ölüm");
  this.dynamic("country-recovered__label", "Yeni İyileşen");
}

async function generic() {
  var json = await fetch("https://api.covid19api.com/country/turkey");
  var data = await json.json();

  var labels = [];
  var cases = [];
  var deaths = [];
  var recovered = [];

  var logaritmic_cases = [];
  var logaritmic_deaths = [];
  var logaritmic_recovered = [];

  data.map((day, d) => {
    labels.push(
      new moment(day.Date).locale("tr").format("D MMMM YYYY") === null
        ? day.Date
        : new moment(day.Date).locale("tr").format("D MMMM YYYY")
    );
    if (day.Date === "2020-06-24T00:00:00Z") {
      logaritmic_cases.push(1492);
      logaritmic_deaths.push(24);
      logaritmic_recovered.push(1386);
      cases.push(191657);
      deaths.push(5025);
      recovered.push(164234);
    } else {
      if (day.Date === "2020-06-25T00:00:00Z") {
        logaritmic_cases.push(1458);
        logaritmic_deaths.push(21);
        logaritmic_recovered.push(1472);
      } else {
        logaritmic_cases.push(
          data[d - 1] !== undefined ? day.Confirmed - data[d - 1].Confirmed : 0
        );
        logaritmic_deaths.push(
          data[d - 1] !== undefined ? day.Deaths - data[d - 1].Deaths : 0
        );
        logaritmic_recovered.push(
          data[d - 1] !== undefined ? day.Recovered - data[d - 1].Recovered : 0
        );
      }

      cases.push(day.Confirmed);
      deaths.push(day.Deaths);
      recovered.push(day.Recovered);
    }
  });

  data = data.reverse();

  new Chart(
    document.getElementById("stats"),
    GraphData(labels, [
      {
        data: cases,
        backgroundColor: "transparent",
        borderColor: "#9B803B",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "Vakalar",
      },
      {
        data: deaths,
        backgroundColor: "transparent",
        borderColor: "#92274D",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "Ölümler",
      },
      {
        data: recovered,
        backgroundColor: "transparent",
        borderColor: "#2B9348",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "İyileşenler",
      },
    ])
  );

  new Chart(
    document.getElementById("logaritmic"),
    GraphData(labels, [
      {
        data: logaritmic_cases,
        backgroundColor: "transparent",
        borderColor: "#9B803B",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "Vakalar",
      },
      {
        data: logaritmic_deaths,
        backgroundColor: "transparent",
        borderColor: "#92274D",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "Ölümler",
      },
      {
        data: logaritmic_recovered,
        backgroundColor: "transparent",
        borderColor: "#2B9348",
        pointRadius: 5,
        pointBorderColor: "transparent",
        borderWidth: 2,
        label: "İyileşenler",
      },
    ])
  );

  $.ajax({
    url: "https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json",
    success: function (data) {
      var country = JSON.parse(data);
      var temporary = [];

      for (var k in country) {
        temporary.push(country[k]);
      }

      country = temporary;
      country = country.reverse();

      country.map((day, d) => historical(day, d, country));
    },
  });
}

async function compare() {
  const countries = await this.countries();
  var world = countries.Global;

  this.dynamic("compare-world-total-cases", this.commas(world.TotalConfirmed));
  this.dynamic(
    "compare-world-new-cases",
    "+" + this.commas(world.NewConfirmed)
  );
  this.dynamic("compare-world-total-deaths", this.commas(world.TotalDeaths));
  this.dynamic("compare-world-new-deaths", "+" + this.commas(world.NewDeaths));
  this.dynamic(
    "compare-world-total-recovered",
    this.commas(world.TotalRecovered)
  );
  this.dynamic(
    "compare-world-new-recovered",
    "+" + this.commas(world.NewRecovered)
  );
}

function summary(countries) {
  this.row(countries.Global);
  countries.Countries.map(function (country) {
    this.row(country);
  });
}

async function engine() {
  this.total();
  this.compare();
  this.generic();

  this.countryDataLatest();
  const countries = await this.countries();
  window.ALL_COUNTRIES_SUMMARY = countries;

  $.ajax({
    url: "https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json",
    success: async function (data) {
      var country = JSON.parse(data);
      var temporary = [];

      for (var k in country) {
        temporary.push(country[k]);
      }

      country = temporary;

      window.COUNTRY_FETCHED_LAST = country;

      summary(window.ALL_COUNTRIES_SUMMARY);
    },
  });

  this.dynamic("world-total", countries.Global.TotalConfirmed, true);
  this.dynamic("world-summary-total", countries.Global.TotalConfirmed, true);
  this.dynamic(
    "world-summary-total-deaths",
    countries.Global.TotalDeaths,
    true
  );
  this.dynamic(
    "world-summary-total-recovered",
    countries.Global.TotalRecovered,
    true
  );
}

function countryDataLatest(type) {
  $.ajax({
    url: "https://raw.githubusercontent.com/ozanerturk/covid19-turkey-api/master/dataset/timeline.json",
    success: function (data) {
      var country = JSON.parse(data);
      var temporary = [];

      for (var k in country) {
        temporary.push(country[k]);
      }

      country = temporary;
      let _totalDeaths = 0;
      let _totalPatients = 5440368;
      let _totalRecovered = 5310709;
      let _recovered = 0;
      let _patients = 0;

      for (let c = 0; c < country.length; c++) {
        _totalDeaths += Number(country[c].deaths);
      }
      for (let c = 480; c < country.length; c++) {
        _patients += Number(country[c].cases);
        _recovered += Number(country[c].recovered);
      }
      _totalPatients += _patients;
      _totalRecovered += _recovered;

      window.TURKEY_TOTALCASE = _totalPatients;
      window.TURKEY_TOTALDEATHS = _totalDeaths;

      window.TURKEY_CASE = country[country.length - 1].cases;
      window.TURKEY_DEATHS = country[country.length - 1].deaths;

      document.getElementById("dinamic--date-total").innerHTML =
        commas(_totalPatients);
      dynamic("total", _totalPatients, true);
      dynamic("total-deaths", _totalDeaths - 1, true);
      dynamic("total-recovered", _totalRecovered, true);

      dynamic("compare-country-total-cases", commas(window.TURKEY_TOTALCASE));
      dynamic("compare-country-new-cases", "+" + commas(window.TURKEY_CASE));
      dynamic(
        "compare-country-total-deaths",
        commas(window.TURKEY_TOTALDEATHS)
      );
      dynamic("compare-country-new-deaths", "+" + commas(window.TURKEY_DEATHS));
      dynamic(
        "compare-country-total-recovered",
        commas(country[country.length - 1].totalRecovered)
      );
      dynamic(
        "compare-country-new-recovered",
        "+" +
          commas(
            country[country.length - 1].totalRecovered -
              country[country.length - 2].totalRecovered
          )
      );
      total(_totalPatients, _totalDeaths, _totalRecovered);
      if (type && type === "total") {
        total(_totalPatients, _totalDeaths, _totalRecovered);
        window.TODAY_BUTTON = new moment();
      } else if (type && type === "yesterday") {
        yesterday(
          country[country.length - 2].cases,
          country[country.length - 2].deaths,
          country[country.length - 2].recovered
        );
      } else if (type && type === "today") {
        today(
          country[country.length - 1].cases,
          country[country.length - 1].deaths,
          country[country.length - 1].recovered
        );
      }
    },
  });
}

$(document).ready(function () {
  current();
});
